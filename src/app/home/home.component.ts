﻿import { Component, OnInit } from '@angular/core';

import { User } from '../_models/index';
import { UserService } from '../_services/index';
import { ActivatedRoute } from '@angular/router';

@Component({
    moduleId: module.id.toString(),
    templateUrl: 'home.component.html',
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users: User[] = [];
    rows: number;
    currentPage: number = 0;
    sort: string = "id";
    desc: boolean = true;
    roleNames = {
      'admin' : 'Администратор',
      'driver' : 'Водитель',
      'legal' : 'Юр.лицо',
      'driver-manager' : 'Водитель менеджер',
    };
    columns = [
      { name: 'id' , descr: '#' },
      { name: 'name' , descr: 'Логин' },
      { name: 'role', descr: 'Роль', format : {func : (data) =>{
        return this.roleNames[data];
      } }},
      { name: 'email' , descr: 'Эл.почта'},
    ];
    constructor(private userService: UserService, private route:ActivatedRoute) {
      route.data
        .subscribe(data => { 
          console.log(data['data']['count']);
          this.users = data['data']['users']; 
          this.rows = data['data']['count'];
        });
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    ngOnInit() {

        //this.loadAllUsers();
    }

    deleteUser(id: number) {
        this.userService.delete(id).subscribe(() => { this.loadAllUsers() });
    }

    public loadAllUsers() {
        this.userService
          .getAll(this.config())
          .subscribe(
            data => {
              console.log(data);
              this.users = data['users'];
              this.rows = data['count']
            });
    }

    setPage(page){
      this.currentPage = page - 1;
      console.log(this.config());
      this.loadAllUsers();
    }

    setSort(data){
      this.sort = data['key'];
      this.desc = data['desc'];
      this.loadAllUsers();
    }

    public config() {
      return {
        params:{
          page:this.currentPage,
          orderBy:this.sort,
          desc:this.desc
        }
      };
    }
}