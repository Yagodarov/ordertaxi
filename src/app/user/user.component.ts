import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import {AuthenticationService} from "../_services/authentication.service";
import { User } from "../_models/user";
import { UserService } from "../_services/user.service";
@Component({
  selector: 'user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  model = new User;
  errors : any = [];
  constructor(private userService: UserService,private router: Router) { }

  submitted = false;

  onSubmit() { 
    this.userService.create(this.model).subscribe(data => {
      this.router.navigate(['users']);
    },
    error => {
      console.log(error['error']);
      this.errors = error['error'];
    });
    console.log(this.model);
  }

  ngOnInit() {

  }

  public getUser()
  {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

}